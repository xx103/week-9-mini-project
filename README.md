# Week 9 Mini-Project: GPT-2 Chatbot Streamlit App

This repository contains a simple yet interactive chatbot web application built with Streamlit and powered by the GPT-2 model from Hugging Face's transformers library. The application allows users to interact with the GPT-2 model by entering prompts, to which the model generates and displays responses.

## Prerequisites

Before you can run this app, you'll need the following installed:
- Python (3.7 or newer)
- Pip (Python package installer)

## Installation

1. **Install required Python packages**: 


```bash
pip install streamlit transformers
```


2. **Install Dependencies**: Install the required Python packages using `pip`.

```bash
pip install -r requirements.txt
```

## Running the App

After installing the prerequisites and the required Python packages, you can implement app.py and run the app locally by executing:

```bash
streamlit run app.py
```

Navigate to the Local URL: http://localhost:8501 provided in the terminal to interact with the chatbot.

![Function overview](screenshot1.png)

## Understanding `app.py`

`app.py` is the main script that launches the Streamlit web app. Here's a breakdown of its contents:

- **Import Statements**: Import Streamlit (`st`) and the `pipeline` function from Hugging Face's `transformers` library.
- **Model Initialization**: The GPT-2 model is loaded with `pipeline('text-generation', model='gpt2')`.
- **Streamlit App Function `main`**:
  - **App Title and Introduction**: Displayed using `st.title` and `st.write`.
  - **User Input**: A text input field where users can type their prompts to the chatbot.
  - **Generate and Display Response**: If there's user input, the GPT-2 model generates a response, which is then displayed on the webpage.
- **Footer**: A small caption at the bottom of the app indicating the technologies used.


```python
import streamlit as st
from transformers import pipeline

# Initialize the model
generator = pipeline('text-generation', model='gpt2')

# Streamlit app
def main():
    # App title and introduction
    st.title('GPT-2 Chatbot')
    st.write('This chatbot uses the GPT-2 model from Hugging Face to generate responses.')

    # User input
    user_input = st.text_input('Say something to the chatbot:')

    # Generate response when there's user input
    if user_input:
        with st.spinner('Thinking...'):
            responses = generator(user_input, max_length=50)
            response = responses[0]['generated_text']
        st.write(response)

    # Footer
    st.caption('Created with Streamlit and Hugging Face Transformers')

# Run the app
if __name__ == '__main__':
    main()
```


## Deploying Your Streamlit App on Streamlit Cloud

Follow these steps to deploy your Streamlit application using Streamlit Cloud, ensuring it's accessible via a web browser through a unique URL.


### Prepare your environment for deployment by creating a `setup.sh` file 

### Deploying Your Streamlit App on Streamlit Cloud 

Follow these steps to deploy your Streamlit application using Streamlit Cloud, ensuring it's accessible via a web browser through a unique URL.

### Create an Account on Streamlit.io

1. Visit [Streamlit.io](https://streamlit.io/) and click on the sign-up button.
2. Follow the prompts to complete your registration.

### Sign In to Streamlit

1. After registering, navigate to the login page.
2. Enter your credentials to sign in to your Streamlit account.

### Deploy Your App on Streamlit Cloud

1. **Navigate to Streamlit Cloud**: Go to the Streamlit Cloud section at [share.streamlit.io](https://share.streamlit.io/).
2. **Start Deployment Process**: Click on the "New app" button.
3. **Select GitHub Repository**: Choose an existing GitHub repository that contains your Streamlit app. Make sure the repository is set to public, or you've allowed Streamlit access to your private repository.
4. **Configure App Settings**: Follow the on-screen instructions to specify your app's deployment settings, including the branch where your app is located and the file path to your Streamlit app within the repository.
5. **Deploy**: Submit the form. Streamlit Cloud will then automatically build and host your app, providing you with a unique URL to access it.

The Streamlit app is now deployed and accessible to anyone with the URL: https://miniproject9-ukdp4yhrzgx3a3gjapqisg.streamlit.app/


![Function overview](screenshot2.png)

